api = 2
core = 7.x

includes[] = drupal-org-core.make

defaults[projects][subdir] = contrib

; Themes
projects[adminimal_theme] = 1.24
projects[bootstrap] = 3.6

; Contrib
projects[apachesolr][version] = 1.8
projects[apachesolr_views][version] = 1.1-beta1
projects[search_api][version] = 1.20
projects[search_api_solr][version] = 1.11
projects[ckeditor][version] = 1.17
projects[adminrole][version] = 1.1
projects[admin_menu][version] = 3.0-rc5
projects[adminimal_admin_menu][version] = 1.6
projects[module_filter][version] = 2.0
projects[ctools][version] = 1.9
projects[date][version] = 2.8
projects[devel][version] = 1.5
projects[ds][version] = 2.11
projects[features][version] = 2.9
projects[field_group][version] = 1.5
projects[multiupload_filefield_widget][version] = 1.13
projects[title][version] = 1.0-alpha7
projects[mailsystem][version] = 2.34
projects[maillog][version] = 1.0-alpha1
projects[file_entity][version] = 2.0-beta2
projects[diff][version] = 3.2
projects[email_registration][version] = 1.2
projects[entity][version] = 1.6
projects[environment_indicator][version] = 2.7
projects[libraries][version] = 2.2
projects[login_destination][version] = 1.1
projects[login_security][version] = 1.9
projects[pathauto][version] = 1.2
projects[redirect][version] = 1.0-rc3
projects[robotstxt][version] = 1.3
projects[scheduler][version] = 1.3
projects[stage_file_proxy][version] = 1.7
projects[strongarm][version] = 2.0
projects[token][version] = 1.6
projects[token_filter][version] = 1.1
projects[ultimate_cron][version] = 2.0-rc1
projects[redis][version] = 2.12
projects[expire][version] = 2.0-rc4
projects[entitycache][version] = 1.5
projects[httprl][version] = 1.14
projects[recacher][version] = 1.0
projects[metatag][version] = 1.7
projects[phone][version] = 1.0-beta1
projects[honeypot][version] = 1.19
projects[google_analytics][version] = 2.3
projects[jquery_update][version] = 3.0-alpha2
projects[views][version] = 3.11
projects[webform][version] = 4.8
projects[webform_ajax][version] = 1.1
projects[webform_phone][version] = 1.21
projects[webform_validation][version] = 1.10
projects[xmlsitemap][version] = 2.2

; Libraries
libraries[predis][download][type] = file
libraries[predis][download][url] = https://github.com/nrk/predis/archive/v0.8.7.zip
