<?php
/**
 * @file
 * Enables modules and site configuration for a Boilerplate site installation.
 */

function system_form_install_select_profile_form_alter(&$form, $form_state) {
  header('Location:install.php?profile=boilerplate&locale=en&continue=1');
}

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function boilerplate_form_install_configure_form_alter(&$form, $form_state) {
  $form['site_information']['site_name']['#default_value'] = $_SERVER['SERVER_NAME'];
}
