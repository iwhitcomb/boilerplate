#!/bin/bash

cd /var/www/html

drush make project.make . --concurrency=5 --no-cache --prepare-install

drush site-install boilerplate --site-name=$SITE_NAME --site-mail=$SITE_MAIL --account-name=$SITE_ACCOUNT_NAME --account-mail=$SITE_ACCOUNT_MAIL --db-url=mysql://$MYSQL_USER:$MYSQL_PASSWORD@mariadb/$MYSQL_DATABASE -y

echo "@include_once('local.settings.php');" >> /var/www/html/sites/default/settings.php

php-fpm
