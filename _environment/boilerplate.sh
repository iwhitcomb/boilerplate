#!/usr/bin/env bash

build() {
  clear
  cwd=$(pwd)
  docker-machine create --driver=$DOCKER_MACHINE_DRIVER $1
  eval $(docker-machine env $1)
  docker-machine ssh $DOCKER_MACHINE_NAME "docker build -t php-drupal:fpm $cwd/_environment/drupal"
  docker-compose --host=$DOCKER_HOST --project-name=default --file=$cwd/_environment/docker-compose.yml up -d
  ip=$(docker-machine ip $DOCKER_MACHINE_NAME)
  cat << EOF

Success! Visit your site at http://$ip

EOF
#  docker-machine ssh $DOCKER_MACHINE_NAME
}

destroy() {
  echo "You will destroy the existing environment."
  read -p "Do you really want to continue? (y/n): " -n 1 -r
  echo
  if [[ ! $REPLY =~ ^[Yy]$ ]]
  then
    exit
  fi

  clear
  docker-machine kill $1
  docker-machine rm $1 -y
}

rebuild() {
  destroy $1
  build $1
}

restart() {
  docker-machine restart $1 -y
}

help() {
  cat << EOF
Launch and manage Boilerplate Docker environments.

Usage:
  sh project.sh [COMMAND] [PROJECT]

Commands:
  build              Build the environment.
  rebuild            Rebuild the environment.
  restart            Restart the environment.
  destroy            Permanently delete the environment.
EOF
}
