#!/usr/bin/env bash

. settings.sh
. _environment/boilerplate.sh

if [[ ! -z "$1" && ! -z "$2" ]]
then

  case  $1  in
    'build')
      build $2
      ;;
    'rebuild')
      rebuild $2
      ;;
    'restart')
      restart $2
      ;;
    'destroy')
      destroy $2
      ;;
    'help')
      help $2
      ;;
    *)
      help
  esac

else

  help

fi
