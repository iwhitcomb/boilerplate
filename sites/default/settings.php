<?php

$conf['cache'] = 1;
$conf['block_cache'] = 1;
$conf['cache_lifetime'] = 900;
$conf['page_cache_maximum_age'] = 3600;
$conf['page_compressio'] = 1;
$conf['preprocess_css'] = 1;
$conf['preprocess_js'] = 1;
$conf['maillog_send'] = 1;

$conf['redis_client_host'] = 'redis';
$conf['redis_client_port'] = 6379;
$conf['redis_client_base'] = 1;
$conf['redis_client_interface'] = 'Predis';
$conf['lock_inc'] = 'sites/all/modules/contrib/redis/redis.lock.inc';
$conf['path_inc'] = 'sites/all/modules/contrib/redis/redis.path.inc';
$conf['cache_backends'][] = 'sites/all/modules/contrib/redis/redis.autoload.inc';
$conf['cache_default_class'] = 'Redis_Cache';
