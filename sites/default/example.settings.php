<?php

/**
 * @file
 * Put site-specific configuration settings in this file.
 *
 * NOTE: If using Docker to build the environment it is not necessary to put database connection string in this file.
 *
 */

$conf['cache'] = 1;
$conf['block_cache'] = 1;
$conf['cache_lifetime'] = 900;
$conf['page_cache_maximum_age'] = 3600;
$conf['page_compressio'] = 1;
$conf['preprocess_css'] = 1;
$conf['preprocess_js'] = 1;
