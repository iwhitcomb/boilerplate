#Boilerplate

Boilerplate is a Drupal starter distribution.

##Requirements
- [Drush](http://www.drush.org/)
- PHP 5.4 or higher

###Optional
- [Docker Machine](https://docs.docker.com/machine/install-machine)
- [VirtualBox](http://www.virtualbox.org)

##Installation

1. Clone the Boilerplate repository
```
git clone git@bitbucket.org:iwhitcomb/boilerplate.git
```

2. Build the Drupal environment
```
cd boilerplate
drush make _environment/drupal/profiles/boilerplate/boilerplate.make .
```

3. [Install Drupal](https://www.drupal.org/docs/7/install)

###(Optional) Docker build

Docker will install all necessary requirements for running a Drupal environment.

1. Clone the Boilerplate repository
```
git clone git@bitbucket.org:iwhitcomb/boilerplate.git
```

2. Create the **_environment/settings.php** file.

3. Optionally, create the Drupal **sites/default/settings.php** file.

3. Build the Docker environment
```
cd boilerplate
sh project.sh build [PROJECT]
```